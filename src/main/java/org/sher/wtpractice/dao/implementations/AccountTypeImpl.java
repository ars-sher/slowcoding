package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.interfaces.AccountTypeInterface;
import org.sher.wtpractice.dto.AccountType;

/**
 * Created by Arseny on 31.08.2014.
 */
public class AccountTypeImpl implements AccountTypeInterface {
    public void save(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.save(accountType);
        return null;
    }); }
    public void update(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.update(accountType);
        return null;
    }); }
    public void delete(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.delete(accountType);
        return null;
    }); }
    public AccountType getById(int number) {
        AccountType res = (AccountType) DBhelper.queryWrapper((Session session) -> {
            return session.get(AccountType.class, number);
        });
        return res;
    }
}
