package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.interfaces.EmailInterface;
import org.sher.wtpractice.dto.Email;

/**
 * Created by Arseny on 30.08.2014.
 */
public class EmailImpl implements EmailInterface{
    public void save(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.save(email);
        return null;
    }); }
    public void update(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.update(email);
        return null;
    }); }
    public void delete(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.delete(email);
        return null;
    }); }
    public Email getById(String id) {
        Email res = (Email) DBhelper.queryWrapper((Session session) -> {
            return session.get(Email.class, id);
        });
        return res;
    }
}
