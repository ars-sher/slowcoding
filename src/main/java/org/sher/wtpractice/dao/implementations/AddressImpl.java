package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.DBoperation;
import org.sher.wtpractice.dao.interfaces.AddressInterface;
import org.sher.wtpractice.dto.Address;

/**
 * Created by Arseny on 29.08.2014.
 */
public class AddressImpl implements AddressInterface {
    public void save(Address address) { DBhelper.queryWrapper((Session session) -> { session.save(address); return null;}); }
    public void update(Address address) { DBhelper.queryWrapper((Session session) -> { session.update(address); return null;}); }
    public void delete(Address address) { DBhelper.queryWrapper((Session session) -> {session.delete(address); return null;}); }
    public Address getById(String id) {
        Address res = (Address) DBhelper.queryWrapper((Session session) -> {
            return session.get(Address.class, id);
        });
        return res;
    }
}
