package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.DBoperation;
import org.sher.wtpractice.dao.interfaces.OfficeInterface;
import org.sher.wtpractice.dto.Office;

/**
 * Created by Arseny on 29.08.2014.
 */
public class OfficeImpl implements OfficeInterface {
    public void save(Office office) { DBhelper.queryWrapper((Session session) -> { session.save(office); return null;}); }
    public void update(Office office) { DBhelper.queryWrapper((Session session) -> { session.update(office); return null;}); }
    public void delete(Office office) { DBhelper.queryWrapper((Session session) -> {session.delete(office); return null;}); }
    public Office getById(int id) {
        Office res = (Office) DBhelper.queryWrapper((Session session) -> {
            return session.get(Office.class, id);
        });
        return res;
    }
}
