package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.interfaces.TransactionInterface;
import org.sher.wtpractice.dto.Transaction;

/**
 * Created by Arseny on 30.08.2014.
 */
public class TransactionImpl implements TransactionInterface {
    public void save(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.save(transaction);
        return null;
    }); }
    public void update(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.update(transaction);
        return null;
    }); }
    public void delete(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.delete(transaction);
        return null;
    }); }
    public Transaction getById(int id) {
        Transaction res = (Transaction) DBhelper.queryWrapper((Session session) -> {
            return session.get(Transaction.class, id);
        });
        return res;
    }
}
