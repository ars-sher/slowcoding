package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.interfaces.AccountInterface;
import org.sher.wtpractice.dto.Account;

/**
 * Created by Arseny on 30.08.2014.
 */
public class AccountImpl implements AccountInterface{
    public void save(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.save(account);
        return null;
    }); }
    public void update(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.update(account);
        return null;
    }); }
    public void delete(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.delete(account);
        return null;
    }); }
    public Account getById(int number) {
        Account res = (Account) DBhelper.queryWrapper((Session session) -> {
            return session.get(Account.class, number);
        });
        return res;
    }
}
