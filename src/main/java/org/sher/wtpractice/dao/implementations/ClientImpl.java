package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.DBoperation;
import org.sher.wtpractice.dao.interfaces.ClientInterface;
import org.sher.wtpractice.dto.Client;

/**
 * Created by Arseny on 29.08.2014.
 */
public class ClientImpl implements ClientInterface {
    public void save(Client client) { DBhelper.queryWrapper((Session session) -> { session.save(client); return null;}); }
    public void update(Client client) { DBhelper.queryWrapper((Session session) -> { session.update(client); return null;}); }
    public void delete(Client client) { DBhelper.queryWrapper((Session session) -> {session.delete(client); return null;}); }
    public Client getById(int id) {
        Client res = (Client) DBhelper.queryWrapper((Session session) -> {
            return session.get(Client.class, id);
        });
        return res;
    }
}
