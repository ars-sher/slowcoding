package org.sher.wtpractice.dao.implementations;

import org.hibernate.Session;
import org.sher.wtpractice.dao.DBhelper;
import org.sher.wtpractice.dao.interfaces.PhoneInterface;
import org.sher.wtpractice.dto.Phone;

/**
 * Created by Arseny on 30.08.2014.
 */
public class PhoneImpl implements PhoneInterface {
    public void save(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.save(phone);
        return null;
    }); }
    public void update(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.update(phone);
        return null;
    }); }
    public void delete(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.delete(phone);
        return null;
    }); }
    public Phone getById(String id) {
        Phone res = (Phone) DBhelper.queryWrapper((Session session) -> {
            return session.get(Phone.class, id);
        });
        return res;
    }
}
