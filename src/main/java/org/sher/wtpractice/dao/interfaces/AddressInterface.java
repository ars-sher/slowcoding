package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Address;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface AddressInterface {
    void save(Address address);
    void update(Address address);
    void delete(Address address);
    Address getById(String id);
}
