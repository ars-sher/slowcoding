package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Account;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface AccountInterface {
    void save(Account account);
    void update(Account account);
    void delete(Account account);
    Account getById(int number);
}
