package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Email;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface EmailInterface {
    void save(Email email);
    void update(Email email);
    void delete(Email email);
    Email getById(String id);
}
