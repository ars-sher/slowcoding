package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Phone;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface PhoneInterface {
    void save(Phone phone);
    void update(Phone phone);
    void delete(Phone phone);
    Phone getById(String id);
}
