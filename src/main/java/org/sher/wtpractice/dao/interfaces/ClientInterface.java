package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Client;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface ClientInterface {
    void save(Client client);
    void update(Client client);
    void delete(Client client);
    Client getById(int id);
}
