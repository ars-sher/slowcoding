package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.AccountType;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface AccountTypeInterface {
    void save(AccountType accountType);
    void update(AccountType accountType);
    void delete(AccountType accountType);
    AccountType getById(int id);
}
