package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Office;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface OfficeInterface {
    void save(Office office);
    void update(Office office);
    void delete(Office office);
    Office getById(int id);
}
