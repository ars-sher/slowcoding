package org.sher.wtpractice.dao.interfaces;

import org.sher.wtpractice.dto.Transaction;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface TransactionInterface {
    void save(Transaction transaction);
    void update(Transaction transaction);
    void delete(Transaction transaction);
    Transaction getById(int id);
}
