package org.sher.wtpractice.dao;

import org.hibernate.Session;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface DBoperation {
    Object operation(Session session);
}
