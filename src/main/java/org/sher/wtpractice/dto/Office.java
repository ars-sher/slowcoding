package org.sher.wtpractice.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Arseny on 24.08.2014.
 */


@Entity (name="Office")
public class Office {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) //primary key
    private int ID;
    private String name;
    private String address;

    public Office() {}
    public Office(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Office office = (Office) o;

        if (ID != office.ID) return false;
        if (!address.equals(office.address)) return false;
        if (!name.equals(office.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ID;
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
}
