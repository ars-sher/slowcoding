package org.sher.wtpractice.dto;

/**
 * Created by Arseny on 24.08.2014.
 */

import org.sher.wtpractice.dao.DBhelper;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    private int document;
    private String name;
//    @Temporal(TemporalType.DATE) //only date, no time and timezone
    private Date creationDate;
    @ManyToOne
    @JoinColumn(name = "OfficeID")
    private Office office;
    private Boolean type;

    public Client() {
    }
    public Client(int document, String name, Calendar creationDate, Office office, Boolean type) {
        this.name = name;
        this.document = document;
        this.creationDate = DBhelper.normalizeCal(creationDate);
        this.office = office;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDocument() {
        return document;
    }

    public void setDocument(int document) {
        this.document = document;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;
        if (ID != client.ID) return false;
        if (document != client.document) return false;
        if (creationDate.compareTo(client.creationDate) != 0) return false;
        if (!name.equals(client.name)) return false;
        if (!office.equals(client.office)) return false;
        if (!type.equals(client.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ID;
        result = 31 * result + document;
        result = 31 * result + name.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + office.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}


