package org.sher.wtpractice.dto;

import javax.persistence.*;

/**
 * Created by Arseny on 24.08.2014.
 */

@Entity (name="Address")
public class Address {
    @Id
    @Column(name = "Address")
    private String address;
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private Client client;

    public Address() {}
    public Address(String address, Client client) {
        this.address = address;
        this.client = client;
    }
    public Client getClient() { return client; }
    public void setClient(Client client) { this.client = client; }
    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        if (!address.equals(address1.address)) return false;
        if (!client.equals(address1.client)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = address.hashCode();
        result = 31 * result + client.hashCode();
        return result;
    }
}
