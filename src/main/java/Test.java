import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.sher.wtpractice.dao.implementations.*;
import org.sher.wtpractice.dao.interfaces.*;
import org.sher.wtpractice.dto.*;

import java.util.Date;


/**
 * Created by Arseny on 23.08.2014.
 */

public class Test {
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    public static void oldmain (String args []) {
        //Office office = new Office("Head office", "Moscow");
        //Client client = new Client(4510, new Date(), office, false);
        //Address address = new Address("Velikopopovici", client);
        Office office;
        Client client;
        Address address;
        Transaction transaction = null;
        Account account;
        AccountType accountType;

        AddressInterface ai = new AddressImpl();
        ClientInterface ci = new ClientImpl();
        OfficeInterface oi = new OfficeImpl();
        TransactionInterface ti = new TransactionImpl();
        AccountInterface aci = new AccountImpl();
        AccountTypeInterface ati = new AccountTypeImpl();

        //oi.save(office);
        transaction = ti.getById(1);
        accountType = ati.getById(2);
        client = ci.getById(3);
        //account = new Account(new Date(), client, 100, accountType);
        //
        // aci.save(account);
        System.out.println(transaction.getAmount());
        //address = ai.getById("Velikopopovici");
        //address.setClient(client);
        //ai.update(address);
        //System.out.println(address.getClient().getDocument());
        System.out.println("Hey");
    }
    private static SessionFactory createSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }
}
