package org.sher.wtpractice;

import org.sher.wtpractice.dao.implementations.TransactionImpl;
import org.sher.wtpractice.dao.implementations.AccountImpl;
import org.sher.wtpractice.dao.interfaces.TransactionInterface;
import org.sher.wtpractice.dto.Transaction;
import org.sher.wtpractice.dto.Account;
import org.testng.annotations.Test;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by Arseny on 31.08.2014.
 */
public class TransactionTest {
    TransactionInterface ci = new TransactionImpl();
    Account account = new AccountImpl().getById(1);
    Transaction transaction = null;

    @Test
    public void save() {
        transaction = new Transaction(account, 100, Calendar.getInstance(), (float) 0.1);
        ci.save(transaction);
        Transaction inserted = ci.getById(transaction.getID());
        assertTrue(transaction.equals(inserted));
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        Account newAccount = new AccountImpl().getById(2);
        transaction.setAccount(newAccount);
        ci.update(transaction);
        Transaction updated = ci.getById(transaction.getID());
        assertEquals(newAccount, updated.getAccount());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = transaction.getID();
        ci.delete(transaction);
        Transaction deleted = ci.getById(id);
        assertNull(deleted);
    }


}
