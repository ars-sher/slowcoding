package org.sher.wtpractice;

import org.sher.wtpractice.dao.implementations.ClientImpl;
import org.sher.wtpractice.dao.implementations.OfficeImpl;
import org.sher.wtpractice.dao.interfaces.ClientInterface;
import org.sher.wtpractice.dto.Client;
import org.sher.wtpractice.dto.Office;
import org.testng.annotations.Test;


import java.sql.Timestamp;
import java.util.Calendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class ClientTest {
    ClientInterface ci = new ClientImpl();
    Office office = new OfficeImpl().getById(1);
    Client client = null;

    @Test
    public void save() {
        client = new Client(4529445, "Innokentiy Ivanov", Calendar.getInstance(), office, true);
        ci.save(client);
        Client inserted = ci.getById(client.getID());
        assertEquals(client, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        int newDocument = 43555322;
        client.setDocument(newDocument);
        ci.update(client);
        Client updated = ci.getById(client.getID());
        assertEquals(newDocument, updated.getDocument());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = client.getID();
        ci.delete(client);
        Client deleted = ci.getById(id);
        assertNull(deleted);
    }
}
