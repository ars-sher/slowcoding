package org.sher.wtpractice;

import org.sher.wtpractice.dao.implementations.AccountTypeImpl;
import org.sher.wtpractice.dao.implementations.ClientImpl;
import org.sher.wtpractice.dao.interfaces.AccountTypeInterface;
import org.sher.wtpractice.dao.interfaces.AccountTypeInterface;
import org.sher.wtpractice.dto.Account;
import org.sher.wtpractice.dto.AccountType;
import org.sher.wtpractice.dto.AccountType;
import org.sher.wtpractice.dto.Client;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class AccountTypeTest {
    AccountTypeInterface ati = new AccountTypeImpl();
    AccountType accountType = ati.getById(1);

    @Test
    public void save() {
        accountType = new AccountType(100, 10, true, true);
        ati.save(accountType);
        AccountType inserted = ati.getById(accountType.getID());
        assertEquals(accountType, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        accountType.setAvailableForCorporated(false);
        ati.update(accountType);
        AccountType updated = ati.getById(accountType.getID());
        assertEquals(false, updated.isAvailableForCorporated());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = accountType.getID();
        ati.delete(accountType);
        AccountType deleted = ati.getById(id);
        assertNull(deleted);
    }
}
