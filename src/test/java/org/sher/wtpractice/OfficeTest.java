package org.sher.wtpractice;

import org.sher.wtpractice.dao.implementations.OfficeImpl;
import org.sher.wtpractice.dao.interfaces.OfficeInterface;
import org.sher.wtpractice.dto.Office;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class OfficeTest {
    OfficeInterface oi = new OfficeImpl();
    Office office = null;

    @Test
    public void save() {
        office = new Office("Head", "Lenina, 1");
        oi.save(office);
        Office inserted = oi.getById(office.getID());
        assertEquals(office, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        String newAddress = "ul Svobody, 2";
        office.setAddress(newAddress);
        oi.update(office);
        Office updated = oi.getById(office.getID());
        assertEquals(newAddress, updated.getAddress());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = office.getID();
        oi.delete(office);
        Office deleted = oi.getById(id);
        assertNull(deleted);
    }
}
