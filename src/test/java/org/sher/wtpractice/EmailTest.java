package org.sher.wtpractice;

import org.sher.wtpractice.dao.implementations.EmailImpl;
import org.sher.wtpractice.dao.implementations.ClientImpl;
import org.sher.wtpractice.dao.implementations.OfficeImpl;
import org.sher.wtpractice.dao.interfaces.EmailInterface;
import org.sher.wtpractice.dto.Email;
import org.sher.wtpractice.dto.Client;
import org.sher.wtpractice.dto.Office;
import org.testng.annotations.Test;


import java.sql.Timestamp;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class EmailTest {
    EmailInterface ai = new EmailImpl();
    Client client = new ClientImpl().getById(1);
    Email email = null;

    @Test
    public void save() {
        email = new Email("portnoy@theater.io", client);
        ai.save(email);
        Email inserted = ai.getById(email.getEmail());
        assertEquals(email, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        Client newClient = new ClientImpl().getById(2);
        email.setClient(newClient);
        ai.update(email);
        Email updated = ai.getById(email.getEmail());
        assertEquals(newClient, updated.getClient());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        String ad = email.getEmail();
        ai.delete(email);
        Email deleted = ai.getById(ad);
        assertNull(deleted);
    }
}
