USE BankSystemDB
GO

DROP TABLE Email
DROP TABLE Phone
DROP TABLE [Address]
DROP TABLE [Ttransaction]
DROP TABLE Account
DROP TABLE AccountType
DROP TABLE Client
DROP TABLE Office

CREATE TABLE Office
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	CONSTRAINT [PK_Office] PRIMARY KEY ([ID] ASC)
);

CREATE TABLE Client
(
	ID int IDENTITY(1,1) NOT NULL,
	[Document] int NOT NULL UNIQUE,
	Name [varchar](100) NOT NULL,
	CreationDate datetime NOT NULL,
	OfficeID int NOT NULL,
	[Type] bit NOT NULL, --0 for natural, 1 for corporated person
	CONSTRAINT [PK_Client] PRIMARY KEY ([ID] ASC)
);

ALTER TABLE Client
  ADD CONSTRAINT UQ_Document UNIQUE ([Document], [Type])

CREATE TABLE Phone
(
	[Phone] [varchar](20) NOT NULL,
	ClientID int NOT NULL,
	CONSTRAINT [PK_Phone] PRIMARY KEY (Phone ASC)
)

CREATE TABLE [Address]
(
	[Address] [varchar](100) NOT NULL,
	ClientID int NOT NULL,
	CONSTRAINT [PK_Address] PRIMARY KEY ([Address] ASC)
)

CREATE TABLE Email
(
	Email [varchar](50) NOT NULL,
	ClientID int NOT NULL,
	CONSTRAINT [PK_Email] PRIMARY KEY (Email ASC)
)

CREATE TABLE Account
(
	Number int IDENTITY(1,1) NOT NULL,
	CreationDate datetime NOT NULL,
	ClientID int NOT NULL,
	Balance int NOT NULL,
	--AccNumbForExtraCharges int, --�������� ������� ������������� ����������� �� ��������� �����. NULL - �� ���� ������.  
	AccountTypeID int NOT NULL,
	PaybackDate date,
	CONSTRAINT [PK_Account] PRIMARY KEY (Number ASC)
)

ALTER TABLE Account
ADD CONSTRAINT CK_PaybackDate 
CHECK(NOT(PaybackDate IS NOT NULL AND Balance >= 0));

--��� � ������/�����/��� (������� �� ���� PayoutPeriod) ������ ���� �������� �������, ��������� �� ������� �����
--�� ������ ������ ����� �������. ������� ����� ���� ����������� �� ���� �� ���� ��� �� �����-�� ������ 
--(������� �� ���� AccNumbForExtraCharges). ������ ������� ������� �� ���������, ������� ����� ���� ��������
--��� ������� �� ���� �� ���� � �� ��� ��������� - (DepositPercentage � DepositPercentageOnOtherAccs).
CREATE TABLE AccountType
(
	ID int IDENTITY(1,1) NOT NULL,
	MaxCredit int NOT NULL,
	PaybackDuration int, --client must pay credit in this time (in days)
	--PayoutPeriod tinyint NOT NULL, --0 - yearly, 1 - monthly, 2 - weekly
	--DepositPercentage float,
	--DepositPercentageOnOtherAccs float,
	IsAvailableForNatural bit NOT NULL,
	IsAvailableForCorporated bit NOT NULL,
	CONSTRAINT [PK_AccountType] PRIMARY KEY (ID ASC)
)

ALTER TABLE AccountType
ADD CONSTRAINT CK_Payback 
CHECK(NOT(PaybackDuration IS NULL AND MaxCredit > 0));

CREATE TABLE [Ttransaction]
(
	ID int IDENTITY(1,1) NOT NULL,
	AccountID int NOT NULL,
	Amount int NOT NULL,
	[Datetime] datetime NOT NULL,
	WithdrawPercentage float NOT NULL DEFAULT(0),
	CONSTRAINT [PK_Transaction] PRIMARY KEY (ID ASC)
)

ALTER TABLE Phone
	ADD CONSTRAINT FK_Phone_Client FOREIGN KEY (ClientID)
		REFERENCES Client (ID);

ALTER TABLE Email
	ADD CONSTRAINT FK_Email_Client FOREIGN KEY (ClientID)
		REFERENCES Client (ID);

ALTER TABLE [Address]
	ADD CONSTRAINT FK_Address_Client FOREIGN KEY (ClientID)
		REFERENCES Client (ID);

ALTER TABLE Account
	ADD CONSTRAINT FK_Account_Client FOREIGN KEY (ClientID)
		REFERENCES Client (ID);

ALTER TABLE Account
	ADD CONSTRAINT FK_Account_AccountType FOREIGN KEY (AccountTypeID)
		REFERENCES AccountType (ID);

ALTER TABLE Client
	ADD CONSTRAINT FK_Client_Office FOREIGN KEY (OfficeID)
		REFERENCES Office (ID);

ALTER TABLE [Ttransaction]
	ADD CONSTRAINT FK_Transaction_Account FOREIGN KEY (AccountID)
		REFERENCES Account (Number);
