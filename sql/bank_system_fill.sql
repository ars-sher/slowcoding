USE BankSystemDB
GO

INSERT INTO Office (Name, [Address])
VALUES ('OOO Vector', 'ul Jugashvili, 3'), ('OAO Basis', 'ul Lenina, 4/21'),
	('Roga i kopyta', 'Vernadskogo, 6'), ('Roga i kopyta', 'Vernadskogo, 6');

INSERT INTO Client (Document, CreationDate, OfficeID, [Type])
VALUES (4210443, GETDATE(), 1005, 0), (7910445, GETDATE(), 1006, 1);

INSERT INTO [Address] ([Address], ClientID)
VALUES ('rustavelli, 15', 5), ('svobodi, 21/4', 6);

INSERT INTO [Phone] ([Phone], ClientID)
VALUES ('+79267584628', 4), ('+79056782563', 3);

INSERT INTO [Email] ([Email], ClientID)
VALUES ('desady@rambler.ru, 15', 5), ('svobodi, 21/4', 6);

INSERT INTO AccountType (MaxCredit, PaybackDate, PayoutPeriod, DepositPercentage,
DepositPercentageOnOtherAccs, IsAvailableForNatural, IsAvailableForCorporated)
VALUES (200, GETDATE(), 0, 0.2, 0.1, 1, 0), (200, GETDATE(), 1, 0.2, 0.1, 1, 0);

INSERT INTO Account (CreationDate, ClientID, Balance,
	AccNumbForExtraCharges, AccountTypeID)
VALUES (GETDATE(), 4, 100, 1, 1), (GETDATE(), 5, 576, 1, 2);

INSERT INTO [Transaction] (AccountID, Amount, [Datetime], WithdrawPercentage)
VALUES (1, 100, GETDATE(), 0.1), (1, 100, GETDATE(), 0.2)

